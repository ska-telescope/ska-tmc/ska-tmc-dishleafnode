# Project makefile for a ska-tmc-dishleafnode project. You should normally only need to modify
# CAR_OCI_REGISTRY_USER and PROJECT below.

# CAR_OCI_REGISTRY_HOST, CAR_OCI_REGISTRY_USER and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST (artefact.skao.int) and overwrites
# CAR_OCI_REGISTRY_USER and PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tmc-dishleafnode
CAR_OCI_REGISTRY_HOST:=artefact.skao.int
PROJECT = ska-tmc-dishleafnode
TANGO_HOST ?= tango-databaseds:10000 ## TANGO_HOST connection to the Tango DS
TELESCOPE ?= SKA-mid
MARK ?= ## What -m opt to pass to pytest
# run one test with FILE=acceptance/test_subarray_node.py::test_check_internal_model_according_to_the_tango_ecosystem_deployed
FILE ?= tests## A specific test file to pass to pytest
ADD_ARGS ?= ## Additional args to pass to pytest
PYTHON_LINE_LENGTH=79
# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-tmc-dishleafnode

SKA_TANGO_OPERATOR = true
# HELM_RELEASE is the release that all Kubernetes resources will be labelled
# with
HELM_RELEASE ?= test
HELM_CHARTS_TO_PUBLISH=

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART=test-parent
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
K8S_CHARTS ?= ska-tmc-dishleafnode test-parent## list of charts
K8S_CHART ?= $(HELM_CHART)
PYTHON_SWITCHES_FOR_ISORT ?=

CI_REGISTRY ?= gitlab.com
CUSTOM_VALUES = --set tmc-dishleafnode.dishleafnode.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST=$(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set tmc-dishleafnode.dishleafnode.image.image=$(PROJECT) \
	--set tmc-dishleafnode.dishleafnode.image.registry=$(CI_REGISTRY)/ska-telescope/ska-tmc/$(PROJECT) \
	--set tmc-dishleafnode.dishleafnode.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-tmc/$(PROJECT)/$(PROJECT):$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
TARANTA ?= false
MINIKUBE ?= false ## Minikube or not
FAKE_DEVICES ?= true ## Install fake devices or not
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS

ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.9

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
K8S_TEST_RUNNER = test-runner-$(HELM_RELEASE)

CI_PROJECT_PATH_SLUG ?= ska-tmc-dishleafnode
CI_ENVIRONMENT_SLUG ?= ska-tmc-dishleafnode
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gitlab_values.yaml)

EXIT_AT_FAIL ?= true

PYTHON_TEST_COUNT ?= 1
ifeq ($(MAKECMDGOALS),python-test)
ADD_ARGS += --forked --count=$(PYTHON_TEST_COUNT)
MARK = (not post_deployment and not acceptance)
endif

K8S_TEST_COUNT ?= 1
ifeq ($(MAKECMDGOALS),k8s-test)
ADD_ARGS += --true-context --count=$(K8S_TEST_COUNT)
MARK = $(shell echo $(TELESCOPE) | sed s/-/_/) and (post_deployment or acceptance)
endif

# Applying exit at fail for k8s tests only
ifeq ($(MAKECMDGOALS),k8s-test)
ifeq ($(EXIT_AT_FAIL),true)
ADD_ARGS += -x
endif
endif
CLUSTER_DOMAIN ?= cluster.local

CLUSTER_DOMAIN ?= cluster.local

PYTHON_VARS_AFTER_PYTEST ?= -m '$(MARK)' $(ADD_ARGS) $(FILE)

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set tmc-dishleafnode.telescope=$(TELESCOPE) \
	--set tmc-dishleafnode.deviceServers.mocks.enabled=$(FAKE_DEVICES) \
	--set global.exposeAllDS=false \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set ska-taranta.enabled=$(TARANTA) \
	$(CUSTOM_VALUES)

PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=.:./src \
							TANGO_HOST=$(TANGO_HOST) \
							CLUSTER_DOMAIN=$(CLUSTER_DOMAIN) \

K8S_TEST_TEST_COMMAND = $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) \
						pytest \
						$(PYTHON_VARS_AFTER_PYTEST) ./tests \
						| tee pytest.stdout

-include .make/base.mk
-include .make/k8s.mk
-include .make/python.mk
-include .make/helm.mk
-include .make/oci.mk
-include PrivateRules.mak

cred:
	make k8s-namespace
	curl -s https://gitlab.com/ska-telescope/templates-repository/-/raw/master/scripts/namespace_auth.sh | bash -s $(SERVICE_ACCOUNT) $(KUBE_NAMESPACE) || true

test-requirements:
	@poetry export --without-hashes --with dev --format requirements.txt --output tests/requirements.txt

k8s-pre-test: python-pre-test test-requirements
