# DishLeafNode

This project is developing the DishLeafNode component of Telescope Monitoring 
and Control (TMC) system, for the Square Kilometre Array. Dish Leaf Node is
responsible for monitoring cand controlling the underlying Dish device. 

Documentation
-------------
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-tmc-dishleafnode/badge/?version=latest)](https://developer.skao.int/projects/ska-tmc-dishleafnode/en/latest/)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, and can be better browsed in the SKA development portal:

* [DishLeafNode documentation](https://developer.skatelescope.org/projects/ska-tmc-dishleafnode/en/latest/  "SKA Developer Portal: DishLeafNode documentation")
