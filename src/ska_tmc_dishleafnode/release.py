# -*- coding: utf-8 -*-
#
# This file is part of the DishLeafNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
# pylint: disable=redefined-builtin

"""Release information for Python Package"""

name = """ska_tmc_dishleafnode"""
version = "0.20.0"
version_info = version.split(".")
description = """ The primary responsibility of the Dish
                  Leaf node is to monitor the Dish Master
                  and issue control actions during an observation."""
author = "Team Sahyadri, Team Himalaya"
author_email = "telmgt-internal@googlegroups.com"
license = """BSD-3-Clause"""
url = """https://www.skatelescope.org"""
copyright = """"""
