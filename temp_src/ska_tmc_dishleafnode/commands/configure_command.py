#
# This file is part of the DishLeafNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""
Configure class for DishLeafNode.
"""
# Standard Python imports

# Tango imports
import tango

# Additional import
from ska_tmc_base.commands import BaseCommand
from ska_tmc_common.tango_client import TangoClient
from ska_tmc_common.tango_server_helper import TangoServerHelper
from tango import DevFailed, DevState

from .command_callback import CommandCallBack


class Configure(BaseCommand):
    """
    A class for DishLeafNode's Configure() command.

    Configures the Dish by setting pointing coordinates for a given scan.
    This function accepts the input json and calculate pointing parameters of Dish- Azimuth
    and Elevation Angle. Calculated parameters are again converted to json and fed to the
    dish master.

    """

    def check_allowed(self)->bool:
        """
        Checks whether this command is allowed to be run in the current device state.

        :return: True if this command is allowed to be run in current device state.
        :rtype: boolean
        """
        if self.state_model.op_state in [
            DevState.FAULT,
            DevState.UNKNOWN,
            DevState.DISABLE,
            DevState.INIT,
        ]:
            return False

        return True

    def do(self, argin)->None:
        """
        Method to invoke Configure command on dish.

        :param argin:
            A String in a JSON format that includes pointing parameters of Dish- Azimuth and
            Elevation Angle.

                Example:
                {"pointing":{"target":{"reference_frame":"ICRS","target_name":"Polaris Australis","ra":"21:08:47.92","dec":"-88:57:22.9"}},"dish":{"receiver_band":"1"}}

        return:
            None

        raises:
            DevFailed If error occurs while invoking ConfigureBand<> command on DishMaster or
            if the json string contains invalid data.

        """

        device_data = self.target
        command_name = "Configure"

        try:
            this_server = TangoServerHelper.get_instance()
            self.dish_master_fqdn = ""
            property_value = this_server.read_property("MidDishControl")
            self.dish_master_fqdn = self.dish_master_fqdn.join(property_value)
            json_argument = device_data._load_config_string(argin)
            receiver_band = json_argument["dish"]["receiver_band"]
            self._configure_band(receiver_band)
        except DevFailed as dev_failed:
            self.logger.exception(dev_failed)
            log_message = f"Exception occured while executing the '{command_name}' command."
            this_server.write_attr("activityMessage", log_message, False)
            tango.Except.re_throw_exception(
                dev_failed,
                f"Exception in '{command_name}' command.",
                log_message,
                f"DishLeafNode.{command_name}Command",
                tango.ErrSeverity.ERR,
            )
        except KeyError as key_error:
            raise Exception(
                f"JSON key not found.'{key_error}'in Configure.do()."
            )
        self.logger.info("'%s' command executed successfully.", command_name)

    def _configure_band(self, band)->None:
        """ "Send the ConfigureBand<band-number> command to Dish Master
        
        Args:
        band (int): The band number.

        :raises:
        DevFailed: If there is an error while sending the command.
        :return: None"""
        command_name = f"ConfigureBand{band}"

        try:
            dish_client = TangoClient(self.dish_master_fqdn)
            cmd_ended_cb = CommandCallBack(self.logger).cmd_ended_cb
            dish_client.send_command_async(
                command_name, callback_method=cmd_ended_cb
            )
        except DevFailed as dev_failed:
            raise dev_failed
